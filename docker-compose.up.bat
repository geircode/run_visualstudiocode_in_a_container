cd /d %~dp0
docker rm -f visualstudiocode-1
docker-compose -f docker-compose.yml down --remove-orphans
docker-compose -f docker-compose.yml pull
docker-compose -f docker-compose.yml up -d --remove-orphans
pause
docker exec -it visualstudiocode-1 /bin/bash