# Run Visual Studio Code in a container

Repository: https://bitbucket.org/geircode/run_visualstudiocode_in_a_container 

Goal:

- Run Visual Studio Code in a Container
- Connect to it with Windows GUI
- Debug a Python script

Why?

- Develop and debug code directly from within a Container and its environment
- Isolation
- Run anywhere

I was inspired by this article https://nickjanetakis.com/blog/using-wsl-and-mobaxterm-to-create-a-linux-dev-environment-on-windows that explains how to setup a similar environment using WSL.



Prerequisites:

- Install [Docker Desktop](https://www.docker.com/products/docker-desktop) on Windows



### TL;TR; steps:

- Install Windows X-Terminal => https://mobaxterm.mobatek.net/
- Run “**docker-compose.up.bat**“ script found in the git repo at https://bitbucket.org/geircode/run_visualstudiocode_in_a_container/src/master/ 
- Find “**TestHelloService.py**” and push F5 to start a debugging this very simple Python app



## Install the Windows X-Terminal: MobaXterm

To be able to see Visual Studio Code on Windows, it needs a X-terminal server that the container can connect to and show/render the GUI.

- Install https://mobaxterm.mobatek.net/


![mobaXterm](wiki/files/mobaXterm.png)

Here you see the X server running in the icon up in the right corner.

Now Windows is ready to accept incoming X-terminal connections from any linux environment.

## Start the Visual Studio Code container

* Get the source code and build the Container yourself:

~~~bash
git clone https://bitbucket.org/geircode/run_visualstudiocode_in_a_container
~~~



* Run the convenience script: "docker-compose.up.bat"

```bash
cd /d %~dp0
docker rm -f visualstudiocode-1
docker-compose -f docker-compose.yml down --remove-orphans
docker-compose -f docker-compose.yml up -d --build --remove-orphans
pause
docker exec -it visualstudiocode-1 /bin/bash
```

This script will build the Docker Container Image if it does not exist and start up the container.

![image-20190825-104223](wiki/files/image-20190825-104223.png)

When the container is starting up, you will get a lot of confirmation boxes from MobaXterm. Click on 'yes' on all of them, and behold:

![2019_08_25_13_25_16_Welcome_Visual_Studio_Code_c03dd9040189](wiki/files/2019_08_25_13_25_16_Welcome_Visual_Studio_Code_c03dd9040189.png)

This is a fully functional Visual Studio Code instance running inside the container!

Login to the container using bash

```bash
docker exec -it visualstudiocode-1 /bin/bash
```

and run "top" to see running processes.

![2019_08_25_13_28_23_dev_c03dd9040189_](wiki/files/2019_08_25_13_28_23_dev_c03dd9040189_.png)

Code is running and connected to the X-terminal in Windows.



## Debug a simple Python application from within

First, open a folder in Visual Studio Code:

![Open_folder](wiki/files/Open_folder.png)

- Open the "app" folder and click "OK"

![2019_08_25_13_40_00_Welcome_app_Visual_Studio_Code_6f239c7462d1](wiki/files/2019_08_25_13_40_00_Welcome_app_Visual_Studio_Code_6f239c7462d1.png)

- Expand the "python\tests" directory and open the file "**TestHelloService.py**"
- Visual Studio Code will ask you to install the "Python" plugin. Yes, please.
- Hit F5 to start debugging this Python application



You will encounter this popup:

![1566735568639](wiki/files/1566735568639.png)

Click on "Allow" so that the Python plugin will be able to find the Python modules in this repository.



### How to allow Visual Studio Code to start up without the confirmation popups

Go to MobXterm and open **Settings**, then **X11**:

![Allowing_full_access_x11](wiki/files/Allowing_full_access_x11.png)

- Select "**Full**" to allow Visual Studio Code to start up without the confirmation hassle
- Restart/Reset the Container by running the "docker-compose.up.bat" script again, and see how Visual Studio Code starts up quickly and without any popups

## Conclusion / Last notes

Lots of small stuff has been configured to make this work properly. Explore the Dockerfile and various settings/config files in the repository. In particular the **DISPLAY** environment variable.

This is only the beginning. Any other Linux app with a GUI can be opened this way. It's also possible to automate most of the manual steps in this how-to.

This is some powerful stuff. It's possible to create shared development container images with preinstalled stuff on it. You can even run it and attach remotely from another Visual Studio Code using the Live Share. You can commit and upload the image, download it to your other development computers and start coding immediately. Create volumes and/or mount local directory into the container. There are a lot of options:)

